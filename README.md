# desktop-tools

Ansible role that installs various software geared towards desktop GUI use.

Might want to consider managing the mimetype database, which is controlled by one or either of
```
~/.local/share/applications/mimeapps.list
~/.local/share/applications/mimeinfo.cache
/usr/share/applications/mimeinfo.cache
/usr/share/applications/defaults.list
```
Not sure whether list/cache take precedence, but it seems like the path in $HOME
takes precedence over system-level path.


## Audio editors

A long time ago I uninstalled Audacity after hearing about the poor decisions of the project's
new owners in 2021 and installed Traverso instead.
But perhaps the fork Tenacity would be a better replacement.

+ https://arstechnica.com/gadgets/2021/07/no-open-source-audacity-audio-editor-is-not-spyware
+ https://github.com/audacity/audacity/discussions/880
+ https://www.zdnet.com/article/audacity-reverses-course-on-plans-to-add-opt-in-telemetry-after-outcry
+ https://fosspost.org/audacity-is-now-a-spyware
+ https://en.wikipedia.org/wiki/Audacity_(audio_editor)#Reception


I am aware of a few other audio editors, FOSS or otherwise:

+ LMMS ([Flatpak](https://flathub.org/apps/io.lmms.LMMS))
+ Ardour 
+ Reaper (free trial, subscription-based)
+ Rosegarden
+ Qtractor
+ Mixxx
+ [Ocenaudio](https://www.ocenaudio.com), only for MS Windows

+ https://en.wikipedia.org/wiki/Comparison_of_digital_audio_editors
+ https://www.makeuseof.com/best-audio-editors-for-linux 
+ https://techwiser.com/open-source-audio-editor
+ https://askubuntu.com/questions/161766/which-is-the-best-multitrack-recording-software
+ https://sound.stackexchange.com/questions/25988/is-there-an-equivalent-to-audacitys-noise-reduction-effect-in-other-daw




## Office suites and word editors

Note: LibreOffice is installed from its own Ansible role (see the workstation playbook).

https://opensourcemusings.com/three-alternatives-to-libreoffice-writer

+ Calligra Words (`apt install calligra`)
+ AbiWord (`apt install abiword`)


```
$ sudo apt install calligra abiword
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following additional packages will be installed:
  abiword-common abiword-plugin-grammar calligra-data calligra-libs calligrasheets calligrastage
  calligrastage-data calligrawords calligrawords-data evolution-data-server-common fonts-lyx karbon
  libabiword-3.0 libcamel-1.2-61 libchamplain-0.12-0 libchamplain-gtk-0.12-0 libebook-contacts-1.2-2
  libedataserver-1.2-23 libemf1 libetonyek-0.1-1 libgoffice-0.10-10 libgoffice-0.10-10-common
  libgsf-1-114 libgsf-1-common libical3 libkchart2 libkchart2-l10n libkf5kdelibs4support-data
  libkf5kdelibs4support5 libkf5kdelibs4support5-bin libkf5khtml-bin libkf5khtml-data libkf5khtml5
  libkf5notifyconfig-data libkf5notifyconfig5 liblink-grammar5 libloudmouth1-0 libodfgen-0.1-1 libots0
  libphonenumber7 libplot2c2 libpstoedit0c2a libspnav0 libtelepathy-glib0 libtidy5 libwps-0.4-4
  libwv-1.2-4 libzip4 link-grammar-dictionaries-en minisat okular-backend-odp okular-backend-odt
  pstoedit
Suggested packages:
  khelpcenter texlive wordnet link-grammar-dictionaries-all spacenavd
Recommended packages:
  libqca2-plugin-ossl
The following NEW packages will be installed:
  abiword abiword-common abiword-plugin-grammar calligra calligra-data calligra-libs calligrasheets
  calligrastage calligrastage-data calligrawords calligrawords-data evolution-data-server-common
  fonts-lyx karbon libabiword-3.0 libcamel-1.2-61 libchamplain-0.12-0 libchamplain-gtk-0.12-0
  libebook-contacts-1.2-2 libedataserver-1.2-23 libemf1 libetonyek-0.1-1 libgoffice-0.10-10
  libgoffice-0.10-10-common libgsf-1-114 libgsf-1-common libical3 libkchart2 libkchart2-l10n
  libkf5kdelibs4support-data libkf5kdelibs4support5 libkf5kdelibs4support5-bin libkf5khtml-bin
  libkf5khtml-data libkf5khtml5 libkf5notifyconfig-data libkf5notifyconfig5 liblink-grammar5
  libloudmouth1-0 libodfgen-0.1-1 libots0 libphonenumber7 libplot2c2 libpstoedit0c2a libspnav0
  libtelepathy-glib0 libtidy5 libwps-0.4-4 libwv-1.2-4 libzip4 link-grammar-dictionaries-en minisat
  okular-backend-odp okular-backend-odt pstoedit
0 upgraded, 55 newly installed, 0 to remove and 0 not upgraded.
Need to get 52.0 MB of archives.
After this operation, 211 MB of additional disk space will be used.
```


## Known issues

### rxvt-unicode does not render Arabic filenames correctly

http://software.schmorp.de/pkg/rxvt-unicode.html

Lehmann (the page author) suggests using `mlterm` instead.
Indeed, a simple test demonstrated that `mlterm` successfully renders Arabic filenames where `rxvt-unicode` failed.

The [ArchLinux wiki for `rxvt-unicode`](https://wiki.archlinux.org/index.php/Rxvt-unicode/Tips_and_tricks#Bidirectional_support)
claims it is possible to add bidirectional support for Arabic using
the [bidi extension](https://metacpan.org/pod/distribution/Text-Bidi/misc/bidi).

+ https://metacpan.org/pod/distribution/Text-Bidi/misc/bidi

At the moment, this is not a pressing matter, just nice to have in time.
This is the first time I hear of `mlterm`.

+ https://wiki.ubuntu.com/Mlterm
+ https://github.com/arakiken/mlterm


### Why do we have NVIDIA *Flatpaks*?

```
taha@asks2:~
$ flatpak list | grep nvidia
nvidia-510-108-03	org.freedesktop.Platform.GL.nvidia-510-108-03		1.4	user
nvidia-510-73-05	org.freedesktop.Platform.GL.nvidia-510-73-05		1.4	user
nvidia-510-85-02	org.freedesktop.Platform.GL.nvidia-510-85-02		1.4	user
nvidia-525-125-06	org.freedesktop.Platform.GL.nvidia-525-125-06		1.4	user
nvidia-525-147-05	org.freedesktop.Platform.GL.nvidia-525-147-05		1.4	user
```

I thought nvidia was managed using aptitude?
Where they installed as a result of our `nvidia` role?
Is their version number the same as those specified in our `nvidia` role?
