---

- name: Merge default variable
  ansible.builtin.import_tasks: merge-defaults.yml
  tags:
    - default
    - updates-only

- name: Install software packages common for all workstation form-factors
  ansible.builtin.apt:
    state: present
    name:
      - abiword                # efficient, featureful word processor with collaboration
      - arandr                 # Simple visual front end for XRandR
      - baobab                 # GNOME disk usage analyzer
      # Share one mouse and keyboard between multiple computers
      # https://github.com/debauchee/barrier
      # I have not conclusively identified how to work with Barrier's config file
      # There appears to be some confusion on that front by others too
      # https://github.com/debauchee/barrier/issues/294
      # https://github.com/debauchee/barrier/issues/230
      - barrier                # Share mouse, keyboard and clipboard over the network
      - calligra               # productivity and creative suite
      - desktop-file-utils     # Utilities for .desktop files, including desktop-file-validate
      # dolphin will not start on i3wm, even after installing breeze-icon-theme (no log output)
      # the dolphin flatpak seems to work, though (see tasks/flatpak/flatpaks.yml)
      # - dolphin                # KDE's file manager
      - dunst                  # dmenu-ish notification-daemon
      - easytag                # GTK+ editor for audio file tags
      - elinks                 # advanced text-mode WWW browser
      - eog                    # (eog: Eye of Gnome, aka Gnome image viewer)
      - evince                 # Document (PostScript, PDF) viewer
      - feh                    # imlib2 based image viewer
      - gedit                  # official text editor of the GNOME desktop environment
      # https://ghostwriter.kde.org/
      # https://opensourcemusings.com/3-open-source-markdown-editors
      - ghostwriter            # Distraction-free, themeable Markdown editor
      - gimp                   # GNU Image Manipulation Program
      - gitk                   # visual git revision tree visualizer
      - gnome-disk-utility     # manage and configure disk drives and media
      - gnome-keyring          # GNOME keyring services (daemon and tools)
      - gparted                # GNOME partition editor
      - gpick                  # advanced GTK+ color picker
      # gthumb is quite useful for matching images to search criteria
      - gthumb                 # image thumbnail viewer and browser
      - gvfs                   # userspace virtual filesystem - GIO module
      - imagej                 # Image processing program with a focus on microscopy images
      - inkscape               # vector-based drawing program
      - inotify-tools          # command-line programs providing a simple interface to inotify
      - krita                  # pixel-based image manipulation program
      - libnotify-bin          # sends desktop notifications to a notification daemon
      - links                  # web browser running in text mode
      # links2 creates its own desktop entry during installation
      - links2                 # web browser running in both graphics and text mode
      - meld                   # diff / merge tool
      - minicom                # menu-driven serial communication program (useful for EdgeRouter console)
      # mkvtoolnix installs mkvextract, mkvmerge, and mkvinfo which are useful
      # e.g., to extract SRT from a video file
      # https://askubuntu.com/questions/452268/extract-subtitle-from-mkv-files
      # https://forums.plex.tv/t/      how-to-use-mkvtoolnix-to-extract-srt-subtitle-form-mkv-file/152365/15
      # https://stackoverflow.com/questions/20676906/      is-it-possible-to-extract-subrip-srt-subtitles-from-an-mp4-video-with-ffmpeg
      # https://superuser.com/questions/1527829/extracting-subtitles-from-mkv-file
      - mkvtoolnix             # Set of command-line tools to work with Matroska files
      - mkvtoolnix-gui         # Set of tools to work with Matroska files - GUI frontend
      - mlterm                 # MultiLingual TERMinal
      #- nautilus              # file manager and graphical shell for GNOME
      #- nemo                  # file manager and graphical shell for Cinnamon
      - neverputt              # 3D miniature golf game
      - pass                   # lightweight directory-based password manager
      # https://pdfpc.github.io
      - pdf-presenter-console  # aka pdfpc, multi-monitor presentation tool (ala Keynote) for PDF files
      - pdfsam                 # PDF Split and Merge
      - photocollage           # GUI tool to make photo collage posters (quite useful)
      - picard                 # MusicBrainz audio files tagger (because easytag doesn’t support m4b)
      - picocom                # minimal dumb-terminal emulation program (useful for EdgeRouter console)
      - pinta                  # light paint.net equivalent
      - policykit-1-gnome      # authentication agent for PolicyKit
      # remmina caused apt unmet dependency errors on mkem150
      # looks like remmina on bionic has become outdated, https://remmina.org/how-to-install-remmina/
      # let's install remmina using flatpak instead on bionic
      # - remmina                # VNC viewer (remote desktop client)
      - scite                  # Lightweight GTK-based Programming Editor
      - seahorse               # GNOME front end for GnuPG
      - sqlitebrowser          # GUI editor for SQLite databases
      - traverso               # Multitrack audio recorder and editor
      #- ubuntu-restricted-extras # MP3, Microsoft fonts, Flash plugin, LAME and DVD playback
      - udisks2                # D-Bus service to access and manipulate storage devices
      # w3m needs a desktop entry, otherwise I'll never remember to use it
      - w3m                    # WWW browsable pager with excellent tables/frames support
      - wireshark              # network traffic analyzer - meta-package
      - x11-utils              # X11 utilities
      - x11-xkb-utils          # X11 xkeyboard utilities (provides setxkbmap)
      - x11-xserver-utils      # X server utilities
      - xarchiver              # GTK+ frontend for most used compression formats
      - xclip                  # command line interface to X selections
      - xdg-utils              # desktop integration utilities from freedesktop.org
      - xdotool                # simulate (generate) X11 keyboard/mouse input events
      - xournal                # GTK+ Application for note taking
      - xpdf                   # Portable Document Format (PDF) reader
      - xtightvncviewer        # virtual network computing client software for X
      - zbackup                # Versatile deduplicating backup tool

- name: Install packages only available on Ubuntu bionic
  when: ansible_distribution_release == "bionic"
  ansible.builtin.apt:
    state: present
    name:
      - leafpad                # GTK+ based simple text editor
      - rhythmbox-ampache      # play audio streams from an Ampache server
      - scantailor             # post-processing tool for scanned pages (dewarping, deskewing, etc.)

- name: Install packages only available on Ubuntu jammy
  when: ansible_distribution_release == "jammy"
  ansible.builtin.apt:
    state: present
    name:
      - l3afpad                # Simple text editor forked from Leafpad

- name: Install Abricotine markdown editor
  ansible.builtin.import_tasks: abricotine/main.yml
  when: desktop_tools.abricotine.installed | bool
  tags: abricotine

# alternatives:
# https://github.com/RonnyDo/ColorPicker
# https://flathub.org/apps/details/nl.hjdskes.gcolor3 # flatpak install flathub nl.hjdskes.gcolor3
# https://github.com/Zyten/ColorPicker
- name: Uninstall colour picker tool (snap)
  community.general.snap:
    name: pick-colour-picker # https://github.com/stuartlangridge/ColourPicker
    state: absent

# flatpaks have a tendency to consume many gigabytes of disk space
# /home/taha/.local/share/flatpak (e.g., on wuhan it's 3.1G, and 5.4G on asks2)
- name: Install Flatpaks
  ansible.builtin.import_tasks: flatpak/main.yml
  tags: flatpak
  become: true
  become_user: "{{ ansible_env.USER }}"
  # note that this regex search includes "lxd_workstation" and so on
  # to limit it to only workstation, use `search("^workstation$")`
  when: >
    group_names is search("workstation") or
    ansible_play_name is search("zhutop")

# note, ansible_dns.search no longer returns a string, but a list (of length==1 at present)
- name: Install strategy game on desktops in home LAN
  ansible.builtin.apt:
    state: present
    name:
      - 0ad                   # Real-time strategy game of ancient warfare
  when:
    - group_names is search("^workstation$")
    - ansible_dns.search is defined
    - ansible_dns.search is search("fs1", ignorecase=true)

- name: Install software packages for high-powered desktops
  ansible.builtin.apt:
    state: present
    name:
      - brasero               # CD/DVD burning application for GNOME
  when:
    - group_names is search("^workstation$")
    - ansible_product_name is not search("UDOO")
    - ansible_dns.search is defined
    - ansible_dns.search is search("fs1", ignorecase=true)

# https://github.com/audacity/audacity/issues/1213
# https://news.ycombinator.com/item?id=27727150
- name: Uninstall deprecated programs
  ansible.builtin.apt:
    name:
      - alsamixergui     # redundant
      - audacity         # has turned into potential spyware, see links
      - gnome-calculator # replaced by qalculator and rofi-calc
      - nm-tray          # only created a redundant (and bad-looking) wifi tray applet
      - remmina          # replaced by flatpak because outdated on bionic
    state: absent

- ansible.builtin.import_tasks: log-git-state.yml
  tags:
    - log-git-state
    - updates-only
